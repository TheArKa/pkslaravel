<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $user_id
 * @property integer $umur
 * @property string $bio
 * @property string $alamat
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 */
class Profile extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'umur', 'bio', 'alamat', 'created_at', 'updated_at'];

    /**
     * Belongs to one user
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

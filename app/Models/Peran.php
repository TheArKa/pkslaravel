<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $film_id
 * @property integer $cast_id
 * @property string $nama
 * @property string $created_at
 * @property string $updated_at
 * @property Cast $cast
 * @property Film $film
 */
class Peran extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['film_id', 'cast_id', 'nama', 'created_at', 'updated_at'];

    /**
     * Belongs to one cast
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cast()
    {
        return $this->belongsTo(Cast::class);
    }

    /**
     * Belongs to one film
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function film()
    {
        return $this->belongsTo(Film::class);
    }
}

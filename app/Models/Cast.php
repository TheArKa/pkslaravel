<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $nama
 * @property integer $umur
 * @property string $bio
 * @property string $created_at
 * @property string $updated_at
 * @property Peran[] $perans
 */
class Cast extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['nama', 'umur', 'bio', 'created_at', 'updated_at'];

    /**
     * Has many peran
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function perans()
    {
        return $this->hasMany(Peran::class);
    }
}

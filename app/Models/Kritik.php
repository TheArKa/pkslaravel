<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $user_id
 * @property integer $film_id
 * @property string $content
 * @property integer $point
 * @property string $created_at
 * @property string $updated_at
 * @property Film $film
 * @property User $user
 */
class Kritik extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'film_id', 'content', 'point', 'created_at', 'updated_at'];

    /**
     * Belongs to one film
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function film()
    {
        return $this->belongsTo(Film::class);
    }

    /**
     * Belongs to one user
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

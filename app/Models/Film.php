<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $genre_id
 * @property string $judul
 * @property string $ringkasan
 * @property integer $tahun
 * @property string $poster
 * @property string $created_at
 * @property string $updated_at
 * @property Genre $genre
 * @property Kritik[] $kritiks
 * @property Peran[] $perans
 */
class Film extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['genre_id', 'judul', 'ringkasan', 'tahun', 'poster', 'created_at', 'updated_at'];

    /**
     * Belongs to one genre
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function genre()
    {
        return $this->belongsTo(Genre::class);
    }

    /**
     * Has many kritik
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function kritiks()
    {
        return $this->hasMany(Kritik::class);
    }

    /**
     * Has many peran
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function perans()
    {
        return $this->hasMany(Peran::class);
    }
}

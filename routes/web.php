<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\IndexController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class, 'index'])->name('home');

Route::get('/register', [AuthController::class, 'register'])->name('register');
Route::post('/welcome', [AuthController::class, 'welcome'])->name('welcome');

Route::get('/data-tables', [IndexController::class, 'dataTable'])->name('data-table');

Route::prefix('cast')->name('cast.')->group(function () {
    Route::get('/', [CastController::class, 'index'])->name('index');

    Route::middleware(['auth'])->group(function () {
        Route::get('/create', [CastController::class, 'create'])->name('create');
        Route::post('/', [CastController::class, 'store'])->name('store');
        Route::get('/{cast}/edit', [CastController::class, 'edit'])->name('edit');
        Route::put('/{cast}', [CastController::class, 'update'])->name('update');
        Route::delete('/{cast}', [CastController::class, 'destroy'])->name('destroy');
    });
    Route::get('/{cast}', [CastController::class, 'show'])->name('show');
});

Auth::routes(['verify' => false, 'reset' => false, 'confirm' => false]);
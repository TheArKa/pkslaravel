@extends('layouts.app')

@section('header')
<div class="row mb-2">
    <div class="col-md-12">
        <h1 class="m-0">Home</h1>
    </div>
    <div class="col-md-12">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Laravel</li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
              <div class="tab-content p-0">
                    <h1>Media Online</h1>
        
                    <h3>Sosial Media Developer</h3>
                    <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
        
                    <h3>Benefit Join di Media Online</h3>
                    <p>
                        <ul>
                            <li>Mendapatkan motivasi dari sesama para Developer</li>
                            <li>Sharing knowledge</li>
                            <li>Dibuat oleh calon web developer terbaik</li>
                        </ul>
                    </p>
        
                    <h3>Cara Bergabung ke Media Online</h3>
                    <p>
                        <ol>
                            <li>Mengunjungi Website ini</li>
                            <li>Mendaftar di <a href="{{ route('register') }}">Form Sign Up</a></li>
                            <li>Selesai!</li>
                        </ol>
                    </p>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('layouts.app')

@section('header')
<div class="row mb-2">
    <div class="col-md-12">
        <h1 class="m-0">Register</h1>
    </div>
    <div class="col-md-12">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Register</a></li>
            <li class="breadcrumb-item active">Laravel</li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
              <div class="tab-content p-0">
                <h2>Buat Account Baru</h2>

                <h4>Sign Up Form</h4>

                <form action="{{ route('welcome') }}" method="post">
                    @csrf
                    <p>First Name :</p>
                    <input type="text" name="first_name" id="first_name">
                    <br>

                    <p>Last Name :</p>
                    <input type="text" name="last_name" id="last_name">
                    <br>

                    <p>Gender</p>
                    <input type="radio" name="gender_male" id="gender_male">
                    <label for="gender_male">Male</label>
                    <br>
                    <input type="radio" name="gender_female" id="gender_female">
                    <label for="gender_female">Female</label>

                    <p>Nationality</p>
                    <select name="nationality" id="nationality">
                        <option value="indonesi">Indonesia</option>
                        <option value="australi">Australia</option>
                        <option value="malaysi">Malaysia</option>
                        <option value="singapore">Singapore</option>
                        <option value="wakanda">Wakanda</option>
                    </select>

                    <p>Language Spoken</p>
                    <input type="checkbox" name="language" id="language">
                    <label for="language">Bahasa Indonesia</label>
                    <br>
                    <input type="checkbox" name="language" id="language">
                    <label for="language">English</label>
                    <br>
                    <input type="checkbox" name="language" id="language">
                    <label for="language">Other</label>

                    <p>Bio</p>
                    <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
                    <br>
                
                    <input type="submit" value="Sign Up">

                </form>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
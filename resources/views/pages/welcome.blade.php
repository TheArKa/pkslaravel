@extends('layouts.app')

@section('header')
<div class="row mb-2">
    <div class="col-md-12">
        <h1 class="m-0">Welcome</h1>
    </div>
    <div class="col-md-12">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Welcome</a></li>
            <li class="breadcrumb-item active">Laravel</li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
              <div class="tab-content p-0">
                <h1>Selamat Datang! {{ $first_name . ' ' . $last_name }}</h1>
            
                <h3>Terima kasih telah bergabung di Website kami. Media Belajar kita bersama!</h3>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
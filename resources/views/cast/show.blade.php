@extends('layouts.app')

@section('content')
    <a href="{{ route('cast.index')}}" class="btn btn-primary btn-sm"><-Back</a>
    <h2>Show Cast {{$cast->id}}</h2>
    <h4>{{$cast->nama}}</h4>
    <p>{{$cast->umur}}</p>
    <p>{{$cast->bio}}</p>
@endsection
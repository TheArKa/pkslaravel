@extends('layouts.app')

@section('content')
<div>
    <h2>Edit Cast {{$cast->id}}</h2>
    <form action="{{ route('cast.update', $cast->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" id="nama"
                placeholder="Masukkan Nama">
            @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="umur">Umur</label>
            <input type="number" class="form-control" name="umur" value="{{$cast->umur}}" id="umur"
                placeholder="Masukkan Body">
            @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
            <textarea name="bio" id="bio" cols="30" class="form-control" rows="10" placeholder="Masukkan Bio">{{$cast->bio}}</textarea>
            @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
</div>
@endsection
@extends('layouts.app')

@section('content')
<a href="{{ route('cast.create') }}" class="btn btn-primary mb-3">Tambah</a>
<table class="table">
    <thead class="thead-light">
        <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Actions</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($casts as $key=>$value)
            <tr>
                <td>{{ $key + 1 }}</th>
                <td>{{ $value->nama }}</td>
                <td>{{ $value->umur }}</td>
                <td>{{ $value->bio }}</td>
                <td>
                    <a href="{{ route('cast.show', $value->id) }}" class="btn btn-sm btn-info">Show</a>
                    <a href="{{ route('cast.edit', $value->id) }}" class="btn btn-sm btn-primary">Edit</a>
                    <form action="{{ route('cast.destroy', $value->id) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" onclick="return confirm('Delete?')" class="btn btn-sm btn-danger" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection
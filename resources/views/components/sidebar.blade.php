<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="{{ asset('plugins/adminlte/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('plugins/adminlte/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Alexander Pierce</a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="{{ route('home') }}" class="nav-link {{ \Request::is('/') ? 'active' : ''}}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item {{ \Request::is('*table*') ? 'menu-open' : ''}}">
            <a href="#" class="nav-link {{ \Request::is('*table*') ? 'active' : ''}}">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Table
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('data-table') }}" class="nav-link {{ \Request::is('data-table*') ? 'active' : ''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>
                    DataTable
                  </p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{ route('cast.index') }}" class="nav-link {{ \Request::is('cast*') ? 'active' : ''}}">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Cast
              </p>
            </a>
          </li>
          @if(Auth::user())
            <li class="nav-item" style="bottom: 0;position: fixed;">
              <form action="{{ route('logout') }}" method="post">
              @csrf
              <button type="submit" onclick="return confirm('Logout now?')" class="nav-link btn btn-danger btn-sm text-left text-white">
                <i class="nav-icon fas fa-sign-out-alt"></i>
                <p>
                  Logout
                </p>
              </button>
              </form>
            </li>
          @else
            <li class="nav-item" style="bottom: 0;position: fixed;">
              <a href="{{ route('login') }}" class="nav-link btn btn-primary btn-sm text-left text-white">
                <i class="nav-icon fas fa-sign-in-alt"></i>
                <p>
                  Login
                </p>
              </a>`
            </li>
          @endif
        </ul>
      </nav>
    </div>
  </aside>